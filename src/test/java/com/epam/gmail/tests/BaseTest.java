package com.epam.gmail.tests;

import java.util.UUID;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.epam.gmail.factory.DriverFactory;
import com.epam.gmail.page.LoginPage;
import com.epam.gmail.property.PropertyProvider;
import com.epam.gmail.report.ExtentTestManager;
import com.epam.gmail.utils.Log;
import com.epam.gmail.utils.MailAPI;

public abstract class BaseTest {
	
	protected WebDriver driver;
	protected LoginPage loginPage;
	
	protected static final String PASS = PropertyProvider.getProperty("password");
	protected static final String USER_FIRST = PropertyProvider.getProperty("user_first");
	protected static final String USER_SECOND = PropertyProvider.getProperty("user_second");
	protected static final String SENT_FOLDER = "[Gmail]/Sent Mail";
	protected static final String INBOX_FOLDER = "inbox";	
	protected static final String RANDOM_TXT = UUID.randomUUID().toString();
	
	@Parameters("browser")
	@BeforeMethod(alwaysRun = true)
	public void setUp(String browser){
		driver = DriverFactory.getDriver(browser);
		driver.get("http://www.gmail.com");
		loginPage = new LoginPage(driver);
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(){
		DriverFactory.close();
	}	
	
	@AfterSuite()
	public void afterSuite() {
		MailAPI.deleteAllMailIn(USER_FIRST, PASS, SENT_FOLDER);
		MailAPI.deleteAllMailIn(USER_SECOND, PASS, INBOX_FOLDER);
		ExtentTestManager.close();
		Log.leaveAttach();
	}
}
