package com.epam.gmail.utils;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.gmail.report.ExtentManager;
import com.epam.reportportal.message.ReportPortalMessage;

public class Log {

	private static Logger LOGGER = LogManager.getLogger();
	
	private Log(){
	}

	public static void info(String msg){
		LOGGER.info(msg);
	}
	
	public static void error(String msg){
		LOGGER.error(msg);
	}
	
	public static void warn(String msg){
		LOGGER.warn(msg);
	}

	public static void info(ReportPortalMessage message) {
		LOGGER.info(message);
		
	}
	
	public static void leaveAttach(){
		try {
			ReportPortalMessage message = new ReportPortalMessage(new File(ExtentManager.getLocation()), "Find Extent Report in Attach");
			LOGGER.info(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
