package com.epam.gmail.listener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.epam.gmail.report.ExtentManager;
import com.epam.gmail.report.ExtentTestManager;
import com.epam.gmail.report.Reporter;
import com.epam.gmail.utils.Log;

public class CustomListener implements ITestListener{
	
@Override
public void onTestStart(ITestResult result) {
	Log.info("Teststarted running:"  + result.getMethod().getMethodName() + " at:" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME));
	ExtentTestManager.startTest(result.getMethod().getMethodName(), result.getMethod().getDescription());
}

@Override
public void onTestSuccess(ITestResult result) {
	Log.info(result.getMethod().getMethodName() + "Success");
	Reporter.testPassLog();
	ExtentTestManager.endTest();
	
}

@Override
public void onTestFailure(ITestResult result) {
	Reporter.testFailLog(result.getThrowable().getMessage() + "\n\n" + Arrays.toString(result.getThrowable().getStackTrace()));
	ExtentTestManager.endTest();
	result.getThrowable().printStackTrace();
}

@Override
public void onTestSkipped(ITestResult result) {
	Reporter.testSkipLog();
	ExtentTestManager.endTest();
	
}

@Override
public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	onTestFailure(result);
	}

@Override
public void onStart(ITestContext context) {
	Log.info("Start Execute Automation Tests");
}

@Override
public void onFinish(ITestContext context) {
	Log.info("Passeds: " + context.getPassedTests().size());
	Log.info("Failed:" + context.getFailedTests().size());
	Log.info("Find the Extend Report Here " + ExtentManager.getLocation());	
}
}