package com.epam.gmail.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.epam.gmail.property.PropertyProvider;
import com.epam.gmail.utils.Log;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

public class DriverFactory {

	private static Map<String, Supplier<WebDriver>> driverMap;
	private static Map<Long, WebDriver> driverThreadMap;

	public static WebDriver getDriver(String browser) {
		WebDriver driver;
		if (driverThreadMap.size() <= 2) {
			driver = driverMap.get(browser).get();
			driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
			driver.manage().timeouts().setScriptTimeout(25, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		}
		else{
			throw new RuntimeException("No Empty Space For Driver Create");
		}
		return driver;
	}

	public static WebDriver getDriver() {
		return driverThreadMap.get(getId()) != null ? driverThreadMap.get(getId()) : getDriver("chrome");
	}

	private static synchronized WebDriver createFireFoxDriver() {
		System.setProperty(PropertyProvider.getProperty("gecko"), PropertyProvider.getProperty("gecko_path"));
		Log.info("Opening FireFox Browser");
		driverThreadMap.put(getId(), new FirefoxDriver());
		return driverThreadMap.get(getId());
	}

	private static synchronized WebDriver createChromeDriver() {
		ChromeDriverManager.getInstance().setup();
		Log.info("Opening CHROME Browser");
		driverThreadMap.put(getId(), new ChromeDriver());
		return driverThreadMap.get(getId());
	}

	private static synchronized WebDriver createIEDriver() {
		InternetExplorerDriverManager.getInstance().setup();
		Log.info("Opening IE Browser");
		driverThreadMap.put(getId(), new InternetExplorerDriver());
		return driverThreadMap.get(getId());
	}
	
	public static synchronized void close(){
		Log.info("Close browser");
		driverThreadMap.get(getId()).quit();
	}
	
	private static synchronized Long getId(){
		return Thread.currentThread().getId();
	}

	static {
		driverMap = new HashMap<>();
		driverThreadMap = new HashMap<>();
		driverMap.put("chrome", () -> createChromeDriver());
		driverMap.put("firefox", () -> createFireFoxDriver());
		driverMap.put("IE", () -> createIEDriver());
	}

}
