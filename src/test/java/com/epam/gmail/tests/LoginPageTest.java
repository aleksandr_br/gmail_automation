package com.epam.gmail.tests;

import org.testng.annotations.Test;

import com.epam.gmail.page.LoginPage;
import com.epam.gmail.page.MainPage;
import com.epam.gmail.report.Reporter;
import com.epam.gmail.utils.Assertion;

public class LoginPageTest extends BaseTest {

	@Test(description = "Verify That When Send The Message It Came To In Box folder")
	public void sendMessageAndCheckedInBoxTest(){
		
		Reporter.logStep("Step 1 : Login to Gmail");
		loginPage.fillEmailField(USER_FIRST);
		loginPage.emailNextButton.click();
		loginPage.fillPasswordField(PASS);
		loginPage.passNextButton.click();
		MainPage mainPage = new MainPage(driver);
		
		Reporter.logStep("Step 2 : Send the MSG");
		mainPage.wait.waitForElementIsClickable(mainPage.composeBtn);
		Reporter.pageView("Main Page View");
		mainPage.composeBtn.click();
		Reporter.pageView("Message Window View");
		Assertion.verifyTrue(mainPage.msgTbl.isEnabled(), "Verify That Message Window Is Opened");
		mainPage.fillRecipientsField(USER_SECOND);
		mainPage.fillSubjectField(RANDOM_TXT);
		mainPage.fillMsgField(RANDOM_TXT);
		mainPage.wait.waitForElementIsClickable(mainPage.sendBtn);
		mainPage.sendBtn.click();
		Reporter.pageView("Sent Msg View");
		Assertion.verifyTrue(mainPage.isMsgFrameClosed(), "Verify That Message Window Is Closed");
		Assertion.verifyTrue(mainPage.isMsgSent(), "Verify that the msg is sent");
		
		Reporter.logStep("Step 3 : LogOut");
		mainPage.wait.waitForElementIsClickable(mainPage.userAccountBtn);
		mainPage.userAccountBtn.click();
		Reporter.pageView("User Icon View");
		mainPage.wait.waitForElementIsClickable(mainPage.logoutBtn);
		mainPage.logoutBtn.click();
		loginPage = new LoginPage(driver);
		
		Reporter.logStep("Step 4 : Swith Account");
		Reporter.pageView("Login Page View");
		loginPage.swithAccount();
		loginPage = new LoginPage(driver);
		loginPage.fillEmailField(USER_SECOND)
				.emailNextButton.click();
		loginPage.fillPasswordField(PASS)
				.passNextButton.click();
		mainPage = new MainPage(driver);
		
		Reporter.logStep("Step 5 : Verify That The Msg is present");
		Assertion.verifyTrue(mainPage.checkIncomingMsg(USER_FIRST, RANDOM_TXT, RANDOM_TXT), "Verify that new Msg present in in-box");
	}

}
