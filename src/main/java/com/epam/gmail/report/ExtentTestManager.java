package com.epam.gmail.report;

import java.util.HashMap;
import java.util.Map;

import com.epam.gmail.utils.Log;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentTestManager {
	
    private static ExtentReports extent = ExtentManager.getInstance();
    private static Map<Long, ExtentTest> testMap;
    

    public static ExtentTest getTest() {
        return testMap.get(getId());
    }

    public static void endTest() {
        extent.endTest(testMap.get(getId()));
    }

    public static ExtentTest startTest(String testName, String desc) {
    	testMap.put(getId(), 
    	extent.startTest(testName, "[Description: " + desc + "]"));
        return testMap.get(getId());
    }
    
    public static void close() {
		Log.info("Close Extend Report");
    	extent.flush();
    }
    
    private static Long getId(){
    	return Thread.currentThread().getId();
    }
    
	static {
		testMap = new HashMap<>();
	}
}
