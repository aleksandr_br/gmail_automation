package com.epam.gmail.utils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.epam.gmail.factory.DriverFactory;
import com.epam.gmail.property.PropertyProvider;
import com.epam.reportportal.message.ReportPortalMessage;

public class ScreenShotCapture {

	
public static String getscreenshot() {
		try {
			String screen = PropertyProvider.getProperty("report_dir") + "screenShot/" + LocalDateTime.now().toString().replaceAll(":", "-") + ".jpeg";
			File scrFile = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(screen));
			ReportPortalMessage message = new ReportPortalMessage(new File(screen), "Screen"+LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
			Log.info(message);
			return screen;
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}

	}

}
