package com.epam.gmail.report;

import com.epam.gmail.utils.Log;
import com.epam.gmail.utils.ScreenShotCapture;
import com.relevantcodes.extentreports.LogStatus;

public class Reporter {

	public static void info(String message) {
		Log.info(message);
		ExtentTestManager.getTest().log(LogStatus.INFO, message);
	}

	public static void pageView(String message) {
		Log.info(message);
		ExtentTestManager.getTest().log(LogStatus.INFO, message + "\n" + "<img src = '"
				+ ScreenShotCapture.getscreenshot() + "' width=\"200\" height=\"150\" alt = \"Some Image\"/>");
	}

	public static void logStep(String message) {
		Log.info(message);
		ExtentTestManager.getTest().log(LogStatus.INFO, "<b>" + message + "</b>");
	}

	public static void testFailLog(String errorLog) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "<b>Failed</b>\n<b>Stack Trace :</b>\n" + errorLog);
	}

	public static void testPassLog() {
		ExtentTestManager.getTest().log(LogStatus.PASS, "<b>Success</b>");
	}

	public static void testSkipLog() {
		ExtentTestManager.getTest().log(LogStatus.SKIP, "<b>SKIPED</b>");

	}
}
